# Pentalumen

Small project to play interactively with rotations in 4D.

See instructions below to compile the project.

## Requirements

- C++ compiler (`sudo apt-get install build-essential`)
- opengl (`sudo apt-get install libgl1-mesa-dev`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended: `sudo apt-get install python-pip; sudo pip install conan`)
- conan remotes for jmmut and bincrafters (`conan remote add jmmut_remote https://api.bintray.com/conan/jmmut/commonlibs` and `conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan`)


## Get dependencies, compile and run

Linux:
```
git clone https://bitbucket.org/mutcoll/pentalumen.git
cd pentalumen
mkdir build && cd build
conan install .. --build missing
cmake -G "Unix Makefiles" ..
make
./bin/pentalumen
```

## What you will see

You will see a wireframe of a simplex: a 4D tetrahedron. This figure has 5 equidistand vertices. It seems that there's a vertex in the center (that is closer to other vertices than the distance between outer vertices) but that's just how the projection shows it.

The distance in the 4th dimension (w) is represented with color. Things far away in the positive w are purple, and things far away in the negative w are cyan.

From that you can deduce that things that share the same color are in the same w coordinate, and the distance you see is the actual distance between them. However, if two things have very different color, the distance will look much shorter than it really is. That's the reason why the center vertex has a different color: it is farther away than the others among themselves.

## Controls

mouse: click and drag to rotate the world
mouse wheel: zoom

1: rotate the simplex in y-z
2: rotate the simplex in x-z
3: rotate the simplex in x-y
4: rotate the simplex in x-w
5: rotate the simplex in y-w
6: rotate the simplex in z-w


## TODO

[x] draw vertices
[x] ortonormal projection
[x] rotations
