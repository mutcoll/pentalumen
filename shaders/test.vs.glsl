varying vec2 v_texCoord;
attribute float attrib;
varying float var_attrib;

void main()
{
	var_attrib = attrib;
//    gl_Position = vec4(1.0 +  float(int(attrib>0.5))* attrib, 1.0 + attrib, 1.0, 1.0);
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	//v_texCoord = vec2(gl_MultiTexCoord0);
}

