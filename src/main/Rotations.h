/**
 * @file Rotations.h
 * @author jmmut
 * @date 2018-10-29.
 */

#ifndef PENTALUMEN_ROTATIONS_H
#define PENTALUMEN_ROTATIONS_H

#include <vector>
#include <Eigen/Dense>
#include "utils/exception/StackTracedException.h"
#include "log/log.h"


class Rotations {
public:
    Rotations(double angle, const Eigen::Vector4d &axis);

    std::vector<Eigen::Vector4d> rotate(const std::vector<Eigen::Vector4d> &points) const;
    Eigen::Vector4d rotate(const Eigen::Vector4d &point) const;

private:
    double angle;
    const Eigen::Vector4d &axis;
    Eigen::Matrix4d rotationMatrix;

    bool isZeroOrOne(int index) const;
};


#endif //PENTALUMEN_ROTATIONS_H
