/**
 * @file TestManager.h
 * @author jmmut
 * @date 2017-08-02.
 */

#ifndef PENTALUMEN_TESTMANAGER_H
#define PENTALUMEN_TESTMANAGER_H

#include "events/Dispatcher.h"
#include "events/EventCallThis.h"
#include "manager_templates/FlyerManagerBase.h"
#include "Pentachoron.h"
#include "Rotations.h"

class TestManager: public FlyerManagerBase{
public:
    TestManager( Dispatcher &d, std::shared_ptr< WindowGL> w);

protected:
    virtual void onDisplay( Uint32 ms) override;

    virtual void onKeyChange(SDL_KeyboardEvent &event) override;
    virtual void onKeyPressed(SDL_Keycode keyCode, SDL_Keymod keyMod, SDL_Scancode scanCode) override;

private:
    Pentachoron pentachoron;
    double angle;
    double delta_angle;
    bool positiveDirectionRotation;

    int mouseWheelPressed;

    template <typename T>
    void drawPoints(const std::vector<T> &points) const {
        for (size_t i = 0; i < points.size(); ++i) {
            for (size_t j = i + 1; j < points.size(); ++j) {
                glVertex3dv(points[i].data());
                glVertex3dv(points[j].data());
            }
        }
    }

    void rotatePoints(double deltaAngle, const Eigen::Vector4d &axis);

    void drawAxis() const;
};

#endif //PENTALUMEN_TESTMANAGER_H
