/**
 * @file Pentachoron.cpp
 * @author jmmut
 * @date 2017-08-01.
 */

#include "Pentachoron.h"

const double H2 = sqrt(3/4.0);
const double H2C = sqrt(3/36.0);
const double H3 = sqrt(6/9.0);
const double H3C = sqrt(6/144.0);
const double H4 = sqrt(10/16.0);
const double H4C = sqrt(10/400.0);

Pentachoron::Pentachoron()
        : pointA(0.0, 0.0, 0.0, 0.0),
        pointB(1.0, 0.0, 0.0, 0.0),
        pointC(0.5, H2, 0.0, 0.0),
        pointD(0.5, H2C, H3, 0.0),
        pointE(0.5, H2C, H3C, H4) {

    Eigen::Vector4d center{0.5, H2C, H3C, H4C};
    pointA = pointA - center;
    pointB = pointB - center;
    pointC = pointC - center;
    pointD = pointD - center;
    pointE = pointE - center;
}

const std::vector<Eigen::Vector4d> Pentachoron::getPoints() const {
    return {pointA, pointB, pointC, pointD, pointE};
}

const std::vector<Eigen::Vector3d> Pentachoron::getPoints(size_t collapsedDimension) const {
    std::vector<Eigen::Vector3d> slicedPoints;
    std::vector<Eigen::Vector4d> points{pointA, pointB, pointC, pointD, pointE};
    for (auto &&point : points) {
        std::vector<double> slicedPoint;
        for (size_t i = 0; i < 4; ++i) {
            if (i != collapsedDimension) {
                slicedPoint.push_back(point.data()[i]);
            }
        }
        slicedPoints.push_back(Eigen::Vector3d{slicedPoint.data()});
    }
    return slicedPoints;
}

const std::vector<Eigen::Vector3d> Pentachoron::getPoints(std::vector<size_t> dimensions) const {
    std::vector<Eigen::Vector3d> slicedPoints;
    std::vector<Eigen::Vector4d> points{pointA, pointB, pointC, pointD, pointE};
    for (auto &&point : points) {
        std::vector<double> slicedPoint;
        for (size_t i = 0; i < 3; ++i) {
            slicedPoint.push_back(point.data()[dimensions[i]]);
        }
        slicedPoints.push_back(Eigen::Vector3d{slicedPoint.data()});
    }
    return slicedPoints;
}

void Pentachoron::setPoints(std::vector<Eigen::Vector4d> points) {
    if (points.size() != 5) {
        throw randomize::utils::exception::StackTracedException("Need 5 points, got " + std::to_string(points.size()));
    }
    pointA = points[0];
    pointB = points[1];
    pointC = points[2];
    pointD = points[3];
    pointE = points[4];
}
