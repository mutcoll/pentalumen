/**
 * @file Rotations.cpp
 * @author jmmut
 * @date 2018-10-29.
 */

#include "TestManager.h"
#include "Rotations.h"

Rotations::Rotations(double angle, const Eigen::Vector4d &axis): angle(angle), axis(axis) {
    if (axis[0] + axis[1] + axis[2] + axis[3] != 2
            or not isZeroOrOne(0) or not isZeroOrOne(1)
            or not isZeroOrOne(2) or not isZeroOrOne(2)) {
        throw randomize::utils::exception::StackTracedException(
                "this function only works with simple rotations of a plane (2 axis)");

    }

    auto ifEnabledThenCosineElseOne = [&](int axis1) {
        if (axis[axis1] == 1) {
            return cos(angle);
        } else {
            return 1.0;
        }
    };
    auto ifEnabledThenSineElseZero = [&](int axis1, int axis2) {
        if (axis[axis1] == 1 and axis[axis2] == 1) {
            return sin(angle);
        } else {
            return 0.0;
        }
    };
    rotationMatrix <<
            ifEnabledThenCosineElseOne(0), -ifEnabledThenSineElseZero(0, 1), -ifEnabledThenSineElseZero(0, 2), -ifEnabledThenSineElseZero(0, 3),
            ifEnabledThenSineElseZero(0, 1), ifEnabledThenCosineElseOne(1), -ifEnabledThenSineElseZero(1, 2), -ifEnabledThenSineElseZero(1, 3),
            ifEnabledThenSineElseZero(0, 2), ifEnabledThenSineElseZero(1, 2), ifEnabledThenCosineElseOne(2), -ifEnabledThenSineElseZero(2, 3),
            ifEnabledThenSineElseZero(0, 3), ifEnabledThenSineElseZero(1, 3), ifEnabledThenSineElseZero(2, 3), ifEnabledThenCosineElseOne(3);

    LOG_DEBUG("matrix rows:\n%f, %f, %f, %f\n%f, %f, %f, %f\n%f, %f, %f, %f\n%f, %f, %f, %f"
              , rotationMatrix.coeff(0, 0), rotationMatrix.coeff(1, 0), rotationMatrix.coeff(2, 0), rotationMatrix.coeff(3, 0)
              , rotationMatrix.coeff(0, 1), rotationMatrix.coeff(1, 1), rotationMatrix.coeff(2, 1), rotationMatrix.coeff(3, 1)
              , rotationMatrix.coeff(0, 2), rotationMatrix.coeff(1, 2), rotationMatrix.coeff(2, 2), rotationMatrix.coeff(3, 2)
              , rotationMatrix.coeff(0, 3), rotationMatrix.coeff(1, 3), rotationMatrix.coeff(2, 3), rotationMatrix.coeff(3, 3))
}

bool Rotations::isZeroOrOne(int index) const {
    return axis[index] == 1 or axis[index] == 0;
}

std::vector<Eigen::Vector4d> Rotations::rotate(const std::vector<Eigen::Vector4d> &points) const {
    std::vector<Eigen::Vector4d> rotatedPoints;
    for (const auto &point : points) {
        rotatedPoints.push_back(rotate(point));
    }
    return rotatedPoints;
}

Eigen::Vector4d Rotations::rotate(const Eigen::Vector4d &point) const {
    LOG_DEBUG("before: %f, %f, %f, %f", point[0], point[1], point[2], point[3])
    auto rotated = rotationMatrix * point;
    LOG_DEBUG("after: %f, %f, %f, %f", rotated[0], rotated[1], rotated[2], rotated[3])
    return rotated;
}
