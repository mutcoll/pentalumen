/**
 * @file Drawer.cpp
 * @author jmmut
 * @date 2018-11-13.
 */

#include "Drawer.h"
#include "TestManager.h"

void Drawer::drawWithColorLines(const std::vector<Eigen::Vector4d> &points4) {
    glLineWidth(5);
    glBegin(GL_LINES);
    for (size_t i = 0; i < points4.size(); ++i) {
        for (size_t j = i + 1; j < points4.size(); ++j) {
            glColor3dv(getWColor(points4[i][3]).data());
            glVertex3dv(points4[i].data());
            glColor3dv(getWColor(points4[j][3]).data());
            glVertex3dv(points4[j].data());
        }
    }
    glEnd();
    glLineWidth(1);
}

std::vector<GLdouble> Drawer::getWColor(double w) {
    if (w >= 0) {
        return {0.5, inRange(0.5 - w), 0.5};
    } else {
        return {inRange(0.5 + w), 0.5, 0.5};
    }
}


void Drawer::drawWithColorTubes(const std::vector<Eigen::Vector4d> &points4) {
    for (size_t i = 0; i < points4.size(); ++i) {
        for (size_t j = i + 1; j < points4.size(); ++j) {
            drawTube(points4[i], points4[j]);
        }
    }
}
std::pair<Vector3D, Vector3D> Drawer::getAxisUpAndRightSide(const Vector3D &direction, double magnitude) {
    auto up = Vector3D{0, 1, 0};

    if ((direction ^ up) == Vector3D{0, 0, 0}) {
        up = Vector3D{0, 0, 1};
    }
    auto rightSide = direction ^ up;
    up = direction ^ rightSide;

    up = up.Unit() * magnitude;
    rightSide = rightSide.Unit() * magnitude;
    return {up, rightSide};
}
void Drawer::drawTube(const Eigen::Vector4d &start4, const Eigen::Vector4d &end4) {
    auto start = Vector3D{start4[0], start4[1], start4[2]};
    auto end = Vector3D{end4[0], end4[1], end4[2]};
    auto direction = end - start;
    double lineWidth = 0.01;

    Vector3D up, rightSide;
    std::tie(up, rightSide) = getAxisUpAndRightSide(direction, lineWidth);

    auto startColor = getWColor(start4[3]);
    auto startColorData = startColor.data();
    auto endColor = getWColor(end4[3]);
    auto endColorData = endColor.data();

    glBegin(GL_TRIANGLE_STRIP);

    glColor3dv(startColorData);
    glVertex3fv((start + up + rightSide).v);
    glColor3dv(endColorData);
    glVertex3fv((end + up + rightSide).v);

    glColor3dv(startColorData);
    glVertex3fv((start - up + rightSide).v);
    glColor3dv(endColorData);
    glVertex3fv((end - up + rightSide).v);

    glColor3dv(startColorData);
    glVertex3fv((start - up - rightSide).v);
    glColor3dv(endColorData);
    glVertex3fv((end - up - rightSide).v);

    glColor3dv(startColorData);
    glVertex3fv((start + up - rightSide).v);
    glColor3dv(endColorData);
    glVertex3fv((end + up - rightSide).v);

    glColor3dv(startColorData);
    glVertex3fv((start + up + rightSide).v);
    glColor3dv(endColorData);
    glVertex3fv((end + up + rightSide).v);

    glEnd();
}

double Drawer::inRange(double d) {
    if (d < 0.0) {
        return 0.0;
    } else if (d > 1.0) {
        return 1.0;
    } else {
        return d;
    }
}
