/**
 * @file Pentachoron.h
 * @author jmmut
 * @date 2017-08-01.
 */

#ifndef PENTALUMEN_PENTACHORON_H
#define PENTALUMEN_PENTACHORON_H

#include <vector>
#include <Eigen/Dense>
#include "utils/exception/StackTracedException.h"

class Pentachoron {
public:
    Pentachoron();

    const std::vector<Eigen::Vector4d> getPoints() const;
    const std::vector<Eigen::Vector3d> getPoints(size_t collapsedDimension) const;
    const std::vector<Eigen::Vector3d> getPoints(std::vector<size_t> dimensions) const;
    void setPoints(std::vector<Eigen::Vector4d> points);
private:
    Eigen::Vector4d pointA;
    Eigen::Vector4d pointB;
    Eigen::Vector4d pointC;
    Eigen::Vector4d pointD;
    Eigen::Vector4d pointE;

};

#endif //PENTALUMEN_PENTACHORON_H
