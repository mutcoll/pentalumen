/**
 * @file Drawer.h
 * @author jmmut
 * @date 2018-11-13.
 */

#ifndef PENTALUMEN_DRAWER_H
#define PENTALUMEN_DRAWER_H


#include <Eigen/Dense>
#include "events/Dispatcher.h"
#include "manager_templates/FlyerManagerBase.h"

class Drawer {

public:
    static void drawWithColorLines(const std::vector<Eigen::Vector4d> &points4);

    static std::vector<GLdouble> getWColor(double w);

    static void drawWithColorTubes(const std::vector<Eigen::Vector4d> &points4);
    static void drawTube(const Eigen::Vector4d &start, const Eigen::Vector4d &end);

    static double inRange(double d);

    static std::pair<Vector3D, Vector3D> getAxisUpAndRightSide(const Vector3D &direction, double magnitude);
};


#endif //PENTALUMEN_DRAWER_H
