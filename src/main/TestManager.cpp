/**
 * @file TestManager.cpp
 * @author jmmut
 * @date 2017-08-02.
 */

#include "TestManager.h"
#include "Rotations.h"
#include "Drawer.h"

using std::vector;

TestManager::TestManager(Dispatcher &d, std::shared_ptr<WindowGL> w)
        : FlyerManagerBase( d, w), angle(0), delta_angle(0.1), positiveDirectionRotation(true), mouseWheelPressed(0) {
    //        GLfloat lineWidthRange[2];
    //        glGetFloatv(GL_ALIASED_LINE_WIDTH_RANGE, lineWidthRange);
    //        LOG_EXP(lineWidthRange[1], "%f");
    //        LOG_EXP(lineWidthRange[0], "%f");
}

void TestManager::onDisplay(Uint32 ms) {
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glPushMatrix();

    this->FlyerManagerBase::onDisplay( ms);

    drawAxis();

    const vector<Eigen::Vector4d> points4 = pentachoron.getPoints();
    Drawer::drawWithColorTubes(points4);

    glPopMatrix();
    std::static_pointer_cast< WindowGL>( this->window.lock())->swapWindow();
}

void TestManager::drawAxis() const {
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(100, 0, 0);

    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 100, 0);

    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 100);
    glEnd();
}

void TestManager::onKeyChange(SDL_KeyboardEvent &event) {
    MediumManagerBase::onKeyChange(event);

    switch(event.keysym.sym) {
    case SDLK_LSHIFT:
    case SDLK_RSHIFT:
        if (event.type == SDL_KEYUP) {
            positiveDirectionRotation = true;
        } else {
            positiveDirectionRotation = false;
        }
    }
}

void TestManager::onKeyPressed(SDL_Keycode keyCode, SDL_Keymod keyMod, SDL_Scancode scanCode) {
    FlyerManagerBase::onKeyPressed(keyCode, keyMod, scanCode);
    switch(keyCode) {
    case SDLK_ESCAPE:
        this->dispatcher.endDispatcher();
        break;
    case SDLK_1:
    case SDLK_KP_1: {
        auto axis = Eigen::Vector4d{0, 1, 1, 0};
        if (positiveDirectionRotation) {
            rotatePoints(delta_angle, axis);
        } else {
            rotatePoints(-delta_angle, axis);
        }
    }
        break;
    case SDLK_2:
    case SDLK_KP_2: {
        auto axis = Eigen::Vector4d{1, 0, 1, 0};
        if (positiveDirectionRotation) {
            rotatePoints(delta_angle, axis);
        } else {
            rotatePoints(-delta_angle, axis);
        }
    }
        break;
    case SDLK_3:
    case SDLK_KP_3: {
        auto axis = Eigen::Vector4d{1, 1, 0, 0};
        if (positiveDirectionRotation) {
            rotatePoints(delta_angle, axis);
        } else {
            rotatePoints(-delta_angle, axis);
        }
    }
        break;
    case SDLK_4:
    case SDLK_KP_4: {
        auto axis = Eigen::Vector4d{1, 0, 0, 1};
        if (positiveDirectionRotation) {
            rotatePoints(delta_angle, axis);
        } else {
            rotatePoints(-delta_angle, axis);
        }
    }
        break;
    case SDLK_5:
    case SDLK_KP_5: {
        auto axis = Eigen::Vector4d{0, 1, 0, 1};
        if (positiveDirectionRotation) {
            rotatePoints(delta_angle, axis);
        } else {
            rotatePoints(-delta_angle, axis);
        }
    }
        break;
    case SDLK_6:
    case SDLK_KP_6: {
        auto axis = Eigen::Vector4d{0, 0, 1, 1};
        if (positiveDirectionRotation) {
            rotatePoints(delta_angle, axis);
        } else {
            rotatePoints(-delta_angle, axis);
        }
    }
        break;
    default:
        break;
    }
}

void TestManager::rotatePoints(double deltaAngle, const Eigen::Vector4d &axis) {
    auto rotations = Rotations{deltaAngle, axis};
    auto points4 = pentachoron.getPoints();
    auto rotatedPoints = rotations.rotate(points4);
    pentachoron.setPoints(rotatedPoints);
}
