
#include <iostream>
#include <Eigen/Dense>
#include "TestManager.h"

using Eigen::MatrixXd;
using Eigen::Matrix4d;
using Eigen::Vector4d;


int main(int argc, char **argv) {
    if (argc == 2) {
        LOG_LEVEL(randomize::log::parseLogLevel(argv[1]));
    } else {
        LOG_LEVEL(LOG_INFO_LEVEL);
    }
    Dispatcher dispatcher;
    dispatcher.addEventHandler(
            [&dispatcher](SDL_Event &event) { dispatcher.endDispatcher(); },
            [](SDL_Event &event) { return event.type == SDL_QUIT; });

    std::shared_ptr<WindowGL> window = std::make_shared<WindowGL>();
    window->open();
    window->initGL();
    dispatcher.addEventHandler(
            [&dispatcher, &window](SDL_Event &event) { window->close(); },
            [windowID = window->getWindowID()](SDL_Event &event) {
                return event.type == SDL_WINDOWEVENT
                        && event.window.windowID == windowID
                        && event.window.event == SDL_WINDOWEVENT_CLOSE;
            });

    TestManager manager(dispatcher, window);
    manager.activate();

    dispatcher.startDispatcher();

    exit(0);
    return 0;
}

