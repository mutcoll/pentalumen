/**
 * @file rotations.cpp
 * @author jmmut
 * @date 2018-10-29.
 */

#include <main/Rotations.h>
#include "utils/test/TestSuite.h"

using randomize::utils::test::TestSuite;
using Eigen::Vector4d;

int main() {
    TestSuite({
            [](){
                auto rotations = Rotations(M_PI / 2.0, Vector4d{1, 1, 0, 0});
                auto rotated = rotations.rotate({1, 2, 3, 4});
                ASSERT_EQUALS_OR_THROW(round(rotated[0]), -2);
                ASSERT_EQUALS_OR_THROW(round(rotated[1]), 1);
                ASSERT_EQUALS_OR_THROW(round(rotated[2]), 3);
                ASSERT_EQUALS_OR_THROW(round(rotated[3]), 4);
            },
            [](){
                auto rotations = Rotations(M_PI / 2.0, Vector4d{1, 0, 1, 0});
                auto rotated = rotations.rotate({1, 2, 3, 4});
                ASSERT_EQUALS_OR_THROW(round(rotated[0]), -3);
                ASSERT_EQUALS_OR_THROW(round(rotated[1]), 2);
                ASSERT_EQUALS_OR_THROW(round(rotated[2]), 1);
                ASSERT_EQUALS_OR_THROW(round(rotated[3]), 4);
            },
            [](){
                auto rotations = Rotations(M_PI / 2.0, Vector4d{1, 0, 0, 1});
                auto rotated = rotations.rotate({1, 2, 3, 4});
                ASSERT_EQUALS_OR_THROW(round(rotated[0]), -4);
                ASSERT_EQUALS_OR_THROW(round(rotated[1]), 2);
                ASSERT_EQUALS_OR_THROW(round(rotated[2]), 3);
                ASSERT_EQUALS_OR_THROW(round(rotated[3]), 1);
            },
            [](){
                auto rotations = Rotations(M_PI / 2.0, Vector4d{0, 1, 1, 0});
                auto rotated = rotations.rotate({1, 2, 3, 4});
                ASSERT_EQUALS_OR_THROW(round(rotated[0]), 1);
                ASSERT_EQUALS_OR_THROW(round(rotated[1]), -3);
                ASSERT_EQUALS_OR_THROW(round(rotated[2]), 2);
                ASSERT_EQUALS_OR_THROW(round(rotated[3]), 4);
            },
            [](){
                auto rotations = Rotations(M_PI / 2.0, Vector4d{0, 1, 0, 1});
                auto rotated = rotations.rotate({1, 2, 3, 4});
                ASSERT_EQUALS_OR_THROW(round(rotated[0]), 1);
                ASSERT_EQUALS_OR_THROW(round(rotated[1]), -4);
                ASSERT_EQUALS_OR_THROW(round(rotated[2]), 3);
                ASSERT_EQUALS_OR_THROW(round(rotated[3]), 2);
            },
            [](){
                auto rotations = Rotations(M_PI / 2.0, Vector4d{0, 0, 1, 1});
                auto rotated = rotations.rotate({1, 2, 3, 4});
                ASSERT_EQUALS_OR_THROW(round(rotated[0]), 1);
                ASSERT_EQUALS_OR_THROW(round(rotated[1]), 2);
                ASSERT_EQUALS_OR_THROW(round(rotated[2]), -4);
                ASSERT_EQUALS_OR_THROW(round(rotated[3]), 3);
            },
    }, "rotations");
}
